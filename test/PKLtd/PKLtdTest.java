/**
 *  
 * 
 **/

package PKLtd;

import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * 
 **/

public class PKLtdTest {
    
    private PKLtd instance = new PKLtd();
    
    private PKLtdTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        
        PKLtd expResult = PKLtd.getInstance();
        PKLtd result = PKLtd.getInstance();
      
        assertSame(expResult, result);
    }

    /**
     * Test of addEmployee method, of class PKLtd.
     */
    @Test
    public void testAddEmployee() {
        System.out.println("addEmployee Object Test : \n");
        String id = "KPG0106";
        String accessCode = "MGR001122";
        String firstName = "Keri";
        String lastName = "Gopichand";
        String address = "Triniad and Tobago";
        int empLvl = 5;
        
        instance.addEmployee(id, accessCode, firstName, lastName, address, empLvl);
        
        boolean expResult = true;
       
        boolean result = false;
        
        if (instance.employeeDatabase.size() == 1) {
            result = true;
        }
        assertEquals(expResult, result);
    }
    
    
    
    //private CenturyConference instance = new CenturyConference();
    //private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    
    //public CenturyConferenceTest() {
    //}
}
