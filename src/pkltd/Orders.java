/**
 *  
 * 
 **/

package pkltd;

import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/

public class Orders //creates a class/blueprint for design of users for use throughout application
{ //start of class object
    
    // Variables declaration - required to faciliate data use objects and classes
    private int count = 1; //counter for number setting
    private String orderNumber; //counter for number to be assigned to 
    private String jobTitleID; //The unique identifier for the job type to be conbined wth order number
    private Date orderDate; //The date which the order is created
    private Date dueDate; //The date which the order is created
    private Customer custInfo; //The identifier to the room object
    private String orderDetails; //The status of the room schedule which can either be available or booked.
    // End of variables declaration     
    
    private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    /*
        Constructor of the RoomSchedule class that takes in RoomScheduleID, Date, 
        Room and the status
    */
    
    
    Orders(){
    }
    
    Orders (int count, String orderNumber, String jobTitleID, Date orderDate, Date dueDate,
            Customer custInfo, String orderDetails)
    {
        this.count = count;
        this.orderNumber = orderNumber;
        this.jobTitleID = jobTitleID;
        this.orderDate = orderDate;
        this.dueDate = dueDate;
        this.custInfo = custInfo;
        this.orderDetails = orderDetails;
        
        count+=1;
    }
    
//------------------------------------------------------------------------------
    
    /*
        "get" functions to return information requested by other objects
    */

    public String getOrderNumber(){
        return this.orderNumber;
    }
    
    public String getJobTitle(){
        return this.jobTitleID;
    }
    
    public Date getOrderDate(){
        return this.orderDate;
    }
    
    public Date getDueDate(){
        return this.dueDate;
    }
    
    public Customer getCustInfo(){
        return this.custInfo;
    }
    
    public String getOrderDetails(){
        return this.orderDetails;
    }
    
    
 //------------------------------------------------------------------------------
    
    /*
    
        toString() method use to produce the desired output when the object is 
        called upon
    
        The toString() method returns the string representation of the object.
    
        If you print any object, java compiler internally invokes the toString() 
        method on the object. So overriding the toString() method, returns the 
        desired output, it can be the state of an object etc. depends on your 
        implementation.
    
    
    */
    
    @Override  
    public String toString() { //Returns a string outout of the object.
        return "Customer Order : \n\n" +
                "Order # : " + orderNumber + " Order Type : " + jobTitleID + "\n" +
                "------------------------------------------------------------------" +
                "Customer Information : \n" + custInfo + "\n" +
                "------------------------------------------------------------------" +
                "Order Date : " + orderDate + " : " + 
                "Due Date : " + dueDate + 
                "Order Details : \n" + orderDetails;
                
    }

    void costActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }

    void retailCostVarActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }

    void supAddressVarActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }

    void costingVarActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
    }
    
} // end class
