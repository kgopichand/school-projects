/**
 *  
 * 
 **/

package pkltd;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/


public class Customer  //creates a class/blueprint for design of users for use throughout application
{ //start of class object
    
    // Variables declaration - required to faciliate data use objects and classes
    private String custID;  //identification number for Employee assigned by company
    private String custFirstName; //given name of personnel employed under this company
    private String custLastName; //given name of personnel employed under this company
    private String custAddress; //password created by employee and stored for later use.
    private String custContact; //access level of personnel, will be used to grant access to records
    // End of variables declaration     
   
//------------------------------------------------------------------------------
    
    /*
        Constructors for Customer abstract class, takes in ID and Name
    */
    
    Customer(){
    }
    
    Customer(String custID, String custFirstName, String custLastName, 
            String custAddress, String custContact){
        this.custID = custID;
        this.custFirstName = custFirstName;
        this.custLastName = custLastName;   
        this.custAddress = custAddress;
        this.custContact = custContact;
    }

//------------------------------------------------------------------------------
    
    /*
        "get" functions to return information requested by other objects
    */
    
    public String getCustID(){
        return this.custID;
    }
    
    public String getCustFirstName(){
        return this.custFirstName;
    }
    
    public String getCustLastName(){
        return this.custLastName;
    }
    
    public String getFulltName(){
        return this.custFirstName + " " + this.custLastName;
    }
    
    public String getCustAdd(){
        return this.custAddress;
    }
    
    public String getCustContact(){
        return this.custContact;
    }
    
//------------------------------------------------------------------------------
    
    /*
        "get" functions to return information requested by other objects
    */

    public void setCustID(String custID) {
        this.custID = custID;
    }

    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public void setCustContact(String custContact) {
        this.custContact = custContact;
    }
    
    
    
//------------------------------------------------------------------------------
    
    /*
        toString() function to put out 
    */
        
    
    @Override  
    public String toString() { //Returns a string outout of the object.
        return "Customer Data: " + " \n" + 
                "Customer First Name : " + custFirstName + "\n" +
                "Customer Last Name  : " + custLastName + "\n" +
                "Address : " + custAddress + "\n" +
                "Phone Contact : " + custContact + "\n";
    }
    

} //end whole class
