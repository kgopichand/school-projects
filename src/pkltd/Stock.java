/**
 *  
 * 
 **/

package pkltd;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/

public class Stock //creates a class/blueprint for design of users for use throughout application
{  //start of class object
    
    // Variables declaration - required to faciliate data use objects and classes
    private String productID; //identification number assigned by company for product identification
    private String prodName;  //name of item
    private String StockUnit;   //standard quantity of measurement 
    private int stockAmount;
    private String retailUnit;
    private float retailStockAmt;
    private String serialNumber; //serial number given by maker
    private String modelNumber;  //model number given by maker
    private String supplier;   //name of prodcut maker
    private String supplierID;   //identifier assigned to prodcut maker
    private String supAddress; //maker location
    private String supContact; //maker contact number(s)
    private float cost;  //price paid to purchase item
    private float retailCost;  //price marked up for sale 
    private int reorderLvl;    //a given stock level to alert user to reorder
    private String Notes;
    // End of variables declaration     
    
    /**
     * - productID : String
     * - prodName : String
     * - StockUnit: String 
     * - stockAmount : int
     * - retailUnit : String
     * - retailStockAmt : float
     * - serialNumber : String
     * - modelNumber : String
     * - supplier : String
     * - supContact : String
     * - supAddress : String
     * - cost : float
     * - retailCost : float 
     * - reorderLvl : int
     * - Notes : String
     * 
     **/
    
//------------------------------------------------------------------------------
    
    /*
        Constructors for Stock abstract class, takes in ID and Name
    */
    
    Stock(){
    }
    
    Stock(String productID, String prodName, String StockUnit, int stockAmount, 
            String retailUnit, float retailStockAmt, String serialNumber, 
            String modelNumber, String supplier, String supContact, 
            String supAddress, float cost, float retailCost, int reorderLvl, 
            String Notes, String supplierID){
        
        this.productID = productID;
        this.prodName = prodName;
        this.StockUnit = StockUnit;
        this.stockAmount = stockAmount;
        this.retailUnit = retailUnit;
        this.retailStockAmt = retailStockAmt;
        this.serialNumber = serialNumber;   
        this.modelNumber = modelNumber;
        this.supplier = supplier;
        this.supplierID = supplierID;
        this.supAddress = supAddress;
        this.supContact = supContact;
        this.cost = cost;
        this.retailCost = retailCost;
        this.reorderLvl = reorderLvl;
        this.Notes = Notes;
    }
    

//------------------------------------------------------------------------------
    
    /*
        "get" functions to return information requested by other objects
    */
    
    public String productID(){
        return this.productID;
    }    
    
    public String productName(){
        return this.prodName;
    }    
    
    public String prodStockingUnit(){
        return this.StockUnit;
    }    
    
    public int getStockAmt(){
        return this.stockAmount;
    }
    
    public String getRetailUnit() {
        return this.retailUnit;
    }     
    
    public float getRetailStockAmt(){
        return this.retailStockAmt;
    }    
    
    public String prodmodelNumber(){
        return this.modelNumber;
    } 
    
    public String prodSerialNumber(){
        return this.serialNumber;
    }    
    
    public String productSupplier(){
        return this.supplier;
    }    
    
    public String supplierAddress(){
        return this.supAddress;
    }    
    
    public String supplierContact(){
        return this.supContact;
    }    
   
    public float productCost(){
        return this.cost;
    }    
    
    public float retailCost(){
        return this.retailCost;
    }    
    
    public int reorderLvl(){
        return this.reorderLvl;
    }    
    
    public String getSupplierID(){
        return this.supplierID;
    }    
    
    public String getNotes() {
        return this.Notes;
    }
    
//------------------------------------------------------------------------------
    
    /*
        "set" functions to return information requested by other objects
    */
    public void setProductID(String productID) {
        this.productID = productID;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public void setStockUnit(String StockUnit) {
        this.StockUnit = StockUnit;
    }

    public void setStockAmount(int stockAmount) {
        this.stockAmount = stockAmount;
    }

    public void setRetailUnit(String retailUnit) {
        this.retailUnit = retailUnit;
    }

    public void setRetailStockAmt(float retailStockAmt) {
        this.retailStockAmt = retailStockAmt;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public void setSupAddress(String supAddress) {
        this.supAddress = supAddress;
    }

    public void setSupContact(String supContact) {
        this.supContact = supContact;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setRetailCost(float retailCost) {
        this.retailCost = retailCost;
    }

    public void setReorderLvl(int reorderLvl) {
        this.reorderLvl = reorderLvl;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }

    
//------------------------------------------------------------------------------
    
    /*
        toString() function to put out 
    */        
    
    @Override  
    public String toString() { //Returns a string outout of the object.
        return "Customer Data: " + " \n" + 
                "Customer First Name : " + "\n" +
                "Customer Last Name  :" + "\n" +
                "Address : "  + "\n" +
                "Phone Contact : " + "\n";
    }
    

  
       
}  //end whole class
