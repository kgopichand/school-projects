/**
 *  
 * 
 **/

package pkltd;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/


public class Employees //creates a class/blueprint for design of users for use throughout application
{ //start of class object
    
    // Variables declaration - required to faciliate data use objects and classes
    private String id;
    private String accessCode;  //specially assigned code for employee to retrieve data
    private String firstName; //given name of personnel employed under this company
    private String lastName; //given name of personnel employed under this company
    private String address; //password created by employee and stored for later use.
    private String password;
    private String contact;
    private int empLvl; //access level of personnel, will be used to grant access to records or accounts
    // End of variables declaration     
    
    
//------------------------------------------------------------------------------
    
    /*
        Constructors for User abstract class, takes in ID and Name
    */
    
    Employees(){
    }
    
    Employees(String id, String accessCode, String firstName, String lastName, 
            String address, int empLvl, String password, String contact){
        this.id = id;
        this.accessCode = accessCode;
        this.firstName = firstName;
        this.lastName = lastName;   
        this.address = address;
        this.empLvl = empLvl;
        this.password = password;
        this.contact = contact;
    }
    

//------------------------------------------------------------------------------
    
    /*
        "get" functions to return information requested by other objects
    */    

    public String getId() {
        return id;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getPassword() {
        return password;
    }

    public String getContact() {
        return contact;
    }

    public int getEmpLvl() {
        return empLvl;
    }

        
//------------------------------------------------------------------------------
    
    /*
    "set" functions to return information requested by other objects
     */  
    
    public void setId(String id) { this.id = id;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setEmpLvl(int empLvl) {
        this.empLvl = empLvl;
    }

//------------------------------------------------------------------------------
    /*
    toString() function to put out
     */
    @Override
    public String toString() {
        //Returns a string outout of the object.
        return "Employee Data: " + " \n" + 
                "Employee ID : " + id + "\n" +
                "Employee Code  : " + accessCode + "\n" +
                "First Name : " + firstName + "\n" +
                "Last Name : " + lastName + "\n" +
                "Address : " + address + "\n" +
                "Employee Level : " + empLvl + "\n";
    }
    
}  //end whole class
