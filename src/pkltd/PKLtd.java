/**
 *  
 * 
 **/
package pkltd;

// imports
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/

public class PKLtd //creates a class/blueprint for design of users for use throughout application
{ //start of class object

    private static PKLtd instance = null;
    
    public static PKLtd getInstance()
    {
        if (instance == null) 
        {
            instance = new PKLtd();
        }
        return instance;
    }
    
    HashMap<String, Employees > employeeDatabase = new HashMap<>();
    HashMap<String, Customer> customerDatabase = new HashMap<>();
    HashMap<String, Orders> orderDatabase = new HashMap<>();
    HashMap<String, Stock> stockDatabase = new HashMap<>();
   
    
    
    
    
    //@Override  //override frunction to 
    // Employee hash map setup 
    public boolean addEmployee(String id, String accessCode, String firstName, 
            String lastName, String address, int empLvl, String password, 
            String contact) 
    {
        boolean added = false;
        
        Employees employee = new Employees(id, accessCode, firstName, lastName,
                address, empLvl, password, contact);
        employeeDatabase.put(id, employee);
        
        if (employeeDatabase.containsKey(id)) 
        {
            added = true;
        }
        
        return added;
    }
    
    
    // Customer hash map setup 
    public boolean addCustomer(String custID, String custFirstName, String custLastName, 
            String custAddress, String custContact)
    {
        
        boolean added = false;
        
        Customer cust = new Customer (custID, custFirstName, custLastName, custAddress, custContact);
        customerDatabase.put(custID, cust);
        
        if (employeeDatabase.containsKey(custID)) 
        {
            added = true;
        }
        
        return added;
        
    }
    
    
    // Orders hash map setup 
    public boolean addOrder(int count, String orderNumber, String jobTitleID, Date orderDate, Date dueDate,
            Customer custInfo, String orderDetails)
    {
        
        boolean added = false;
        
        Orders order = new Orders (count, orderNumber, jobTitleID, orderDate, dueDate, custInfo,  orderDetails);
        orderDatabase.put(orderNumber, order);
        
        if (orderDatabase.containsKey(orderNumber)) 
        {
            added = true;
        }
        
        return added;
        
    }
    
    
    // Stock hash map setup 
    public boolean addStock(String productID, String prodName, String StockUnit, int stockAmount, 
            String retailUnit, float retailStockAmt, String serialNumber, 
            String modelNumber, String supplier, String supplierID, String supContact, 
            String supAddress, float cost, float retailCost, int reorderLvl, 
            String Notes)
    {
        
        boolean added = false;
        
        Stock stocks = new Stock ( productID,  prodName,  StockUnit,  stockAmount, 
                retailUnit,  retailStockAmt,  serialNumber, modelNumber,  
                supplier,  supContact, supAddress,  cost,  retailCost,  reorderLvl, 
                Notes,  supplierID);
        stockDatabase.put(productID, stocks);
        
        if (stockDatabase.containsKey(productID)) 
        {
            added = true;
        }
        
        return added;
        
    }
    
    
       
}  //end whole class
