/**
 *  
 * 
 **/

package pkltd;

import java.io.*;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/

public class User implements Serializable //creates a class/blueprint for design of users for use throughout application
{ //start of class object
    
    // Variables declaration - required to faciliate data use objects and classes
    private Employees id;  //ID of type String for the User, who can be either a Client or manager
    private String password; //Name of type String for User, who can be either Client or manager
    // End of variables declaration     
    
    
//------------------------------------------------------------------------------
    
    /*
        Constructors for Invoice generator 
    */
    
    User(Employees id, String password){
        this.id = id;
        this.password = password;
        //this.emp = emp;
    } // end object
        
//------------------------------------------------------------------------------
    
    /*
        "get" functions to return information requested by other objects
    */
    
    public Employees getID(){
        return this.id;
    }
    
    public String getPass(){
        return this.password;
    }
    
    
//------------------------------------------------------------------------------
    
    /*
    
        toString() method use to produce the desired output when the object is 
        called upon
    
        The toString() method returns the string representation of the object.
    
        If you print any object, java compiler internally invokes the toString() 
        method on the object. So overriding the toString() method, returns the 
        desired output, it can be the state of an object etc. depends on your 
        implementation.
    
    
    */
    
    @Override  
    public String toString() { //Returns a string outout of the object.
        return "Employee ID  " + id + "\n" +
                //"Employee Data: " + " \n" 
                "\n";
    }
    
}   //end whole class
