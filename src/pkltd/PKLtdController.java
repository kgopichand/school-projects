/**
 *  
 * 
 **/

package pkltd;

import java.util.Date;

/**
 *
 * @author Keri P. Gopichand
 * ID# 16063811
 * Online & PC ID : Eris00
 * 
 **/


public interface PKLtdController //creates a class/blueprint for design of users for use throughout application
{ //start of class object
    
    public boolean addEmployee(String id, String firstName, String lastName, 
            String password, String empLvl);
    
    public boolean addCustomer(String custID, String custFirstName, String custLastName, 
            String custAddress, String custContact);
    
    public boolean addOrder(int orderNumber, String jobTypeID, Date orderDate, Date dueDate,
            Customer custInfo, String orderDetails);
    
    public boolean addStock(String productID, String prodName, String StockUnit, int stockAmount, 
            String retailUnit, float retailStockAmt, String serialNumber, 
            String modelNumber, String supplier, String supContact, 
            String supAddress, float cost, float retailCost, int reorderLvl, 
            String Notes);
    
    /*
    ----------------------------------------------------------------------------
    */
    
    // Information "getter"
    public Employees getEmpInfo(String firstName, String lastName);
    public Customer getCustInfo(String custFirstName, String custLastName);
    public Orders getOrder(String custFirstName, String custLastName);
    public Stock getItemInfo(String prodName);
    
    
    /*
    
     
    public boolean addManager(String id, String name);
    public boolean addClient(String id, String name, String contactInfo);
    public boolean addRoom(String name, int capacity);
    public Room getRoom(String name);
    public boolean addRoomSchedule(Date date, String room, String status);
    public RoomSchedule getRoomSchedule(int id);
    public User getClient(String id);
    public User getManager(String id);
    public boolean updateRoomScheduleStatus(int id, String status);
    public boolean addBooking(int id, String resources, String notes, 
               String bookingStatus, String clientID, String ManagerID);
 
    public boolean updateBookingStatus(int id, String status);
    public String getRoomScheduleByStatus(String status);
    public String getBookingByStatus(String status);
    public Booking getBookingByID(int id);
    
    public RoomSchedule getRoomScheduleByIdAndStatus(int id, String status);
    
    public Room changeRoomName(String ID, String name);
    public String listRoom();
    
    public Booking getBooking(int id, String status);
    public boolean updateBooking(int bookingID, int ScheduleID, String resources, String notes);
    
    
    */
    
    
    
    
}  //end whole class
